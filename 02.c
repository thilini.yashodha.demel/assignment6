#include<stdio.h>
int FibonacciSeq(int n);

int main(void)
{
    int X;

    printf("Enter a number: ");
    scanf("%d", &X);

    for(int n = 0; n <= X; n++)
    {
        printf("%d ", FibonacciSeq(n));
    }

    return 0;
}

int FibonacciSeq(int num)
{

    if(num == 0 || num == 1)
    {
        return num;
    }

    else
    {
        return FibonacciSeq(num-1) + FibonacciSeq(num-2);
    }

}
